#! /usr/bin/env python

from fastjet import *
from math import pi,sqrt

# priklad 4 vektor, trida fastjet.Pseudojet
# (px, py, pz, E)

vektor1 = PseudoJet(100,0,0,200)
vektor2 = PseudoJet(100,100,0,200)

#vzdalenost mezi 2 4vektory - v uhlovych promennych
#dR^2 = (y1-y2)^2 + (phi1-phi2)^2

#potrebujeme spocitat y1-y2
#potrebujeme phi1-phi2 - pozor, azimutalni uhel jde jen od 0-360 stupnu (0-2pi) radianu
#pouzijte metody PseudoJet.rapidity(), PseudoJet.phi()

# konvenvce dphi = phi1-phi2 aby byla mezi -pi a pi

#jake musi byt px, py,pz,E, aby 2 castice byly mene nez R=0.4 od sebe?

# abychom to nemuseli porad pocitat, tak existuje i PseudoJet.delta_R(PseudoJet)
