#! /usr/bin/env python

from fastjet import *
from ROOT import gRandom, TH2D, TCanvas, gApplication, gStyle

# priklad ctyvektor, trida fastjet.Pseudojet
#udelame si pole (vice prvku stejneho typu) - napr. 100 castic
# python: kolekcecastic = []
# k prvku se da pristupovat pres jeho index (cislovani on 0) castice1 = kolekcecastic[0]
# pres vsechny prvky se da prochazet ruznymi metodami
# do pole se da pridavat (append), odebirat (remove) atd

#smycka (loop), vytvorime 100 castic (ctyrvektoru)
# prozatim jen nahodne zvolime pT, y, phi, m 

# jak zkusit vykreslit?
# vizualizaci se da udelat spousta, abychom nemuseli vymyslet
# pouzijeme sadu nastroju ROOT pouzivanych v casticove fyzice
# tohle je histogram (=graf cetnosti), ale nam pomuze k jednoduche vizualizaci
# na osu x dame rapiditu, na osy y phi jednotlivych castic
# na osu z dame pricnou hybnost pt (osa z by obecne ukazovala cetnost, pro nas vic pt = lepe)

# rozdeleni = TH2D("zobrazenicastic","zobrazenicastic",80,-4,4,63,0,6.28)
#for castice in kolekcecastic:
#    rozdeleni.Fill(castice.rapidity(),castice.phi(),castice.pt())

# zkusime nakreslit na ROOT TCanvas
#novycanvas = TCanvas("obrazek1")
#rozdeleni.Draw("COLZ")

# Dalsi krok muzu popsat osy

# tohle pouzijme, aby python nekoncil program/nezaviral okna
# gApplication.Run()
