#! /usr/bin/env python
from fastjet import *
from ROOT import gRandom, TH2D, TCanvas, gApplication, gStyle

# Zkusme misto fastjetu nadefinovat vlastni anti-kt algoritmus
# v co nejjednodussi podobe (bez ohledu na optimalnost)
# vstup bude kolekce ctyrvektoru, polomer R, minimalni hodnota pT (co jeste bude jet)
# vystupem bude kolekce jetu

# python definice funkce
# def jmeno_funkce(parametry)
#     ....
#     return vystup

# anti-kt:
#  potrebujeme pro kazdou castici i spocitat jeji d_ii = pt^-2
#  pro kazdy par (i,j) potrebujeme spocitat d_ij = min(pt_i ^-2, pt_j ^-2) dR^2/R^2
#  najit nejmensi hodnotu mezi vsemi d_ii a d_ij
#  pokud je to d_ii, tak mame jet a presunemeho do kolekce jetu
#  pokud je to d_ij, tak sparujeme castice i,j do jedne i+j
#  algoritmus bezi dokud zbyvaji nejake vstupni castice

# python smycka: while (len(castice)):

# jak vytvorit 2D pole (matice) pro vsechny d_ii, d_ij
# vzdalenosti = [[None for _ in range(len(castice))] for _ in range(len(castice))]

# pro porovnani s fastjet pouzijeme
#dR = 0.4
#min_pt = 10
#fastjetalgoritmus = JetDefinition(antikt_algorithm, dR)
#clusters = ClusterSequence(vstupnicastice,fastjetalgoritmus)
#jety = sorted_by_pt(clusters.inclusive_jets(10.))
#abychom dostali porovnatelny vystup


