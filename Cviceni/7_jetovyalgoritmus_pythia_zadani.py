#! /usr/bin/env python
# a na zaver bych chtel zkusit pustit jetovy algoritmus na temer realny priklad
# tzv. Monte Carlo generator umi simulovat srazku napr. protonu s protonem
# vybereme tzv. tvrde QCD srazky (hard QCD) ve kterych budou vznikat jety
# vybereme z kazde srazky jen castice, ktere bychom mohli pozorovat v detektoru
# pustime fastjet anti-kt algoritmus jen na ne (stovky castic)
# dostaneme jen nekolik jetu = typicka srazka
# zkusime se podivat na rozdeleni pt, y, phi produkovanych jetu
# z pT, y muzeme dostat (priblizne) hodnotu x1, x2 - zlomku hybnosti protonu ktere nesly puvodni interagujici partony


from fastjet import *
from ROOT import gRandom, TH1D, TCanvas, gApplication, gStyle, gPad
from pythia8 import *
import math

# vygenerujme napr. 10000 udalosti (srazek, eventu)
# nase nastaveni
#poceteventu = 10000
#dR = 0.4
#minpt = 100

# nastaveni Monte Carlo generatoru Pythia
# budeme generovat "Hard QCD"/"tvrde" srazky, ve kterych vznikaji jety

#pythia = Pythia("", False)
#pythia.readString("Beams:eCM = 13600.")
#pythia.readString("HardQCD:all = on")
#pythia.readString("PhaseSpace:pTHatMin = 1000.")
#pythia.readString("PhaseSpace:bias2Selection = on")
#pythii nutno inicializovat
pythia.init()

# vytvoreni 1D histogramu (ROOT), jmeno, nazev, pocet binu na ose x, rozsah od, do
"""
hist_pt  = TH1D("histogram_pt","histogram_pt",50,0,5000)
hist_y   = TH1D("histogram_y","histogram_y",100,-5,5)
hist_phi = TH1D("histogram_phi","histogram_phi",63,0,6.28)

hist_x1  = TH1D("histogram_x1","histogram_x1",50,0,1)
hist_x2  = TH1D("histogram_x2","histogram_x2",50,0,1)

#pro spravne plneni vahou je pro spravne pocitani statisticke chyby pouzit (my budeme ignorovat)
hist_pt.Sumw2()
hist_y.Sumw2()
hist_phi.Sumw2()
hist_x1.Sumw2()
hist_x2.Sumw2()
"""

#smycka pro generovani udalosti:
"""
for iEvent in range(poceteventu):
    if not pythia.next(): continue;
    #tady mame dobry event
"""
#potrebujeme vyfiltrovat castice, ktere muzeme videt v detektoru
#vezmeme jen 'finalni castice' (isFinal()) a castice, ktere nejsou neutrina nebo miony (kody +/-12, +/-13, +/- 14, +/- 16)
"""
        if particle.isFinal() and abs(particle.id())!=12 and abs(particle.id())!=13 and abs(particle.id())!=14 and abs(particle.id())!=16:
            castice.append(PseudoJet(particle.px(),particle.py(),particle.pz(),particle.e()))
"""
# pustme jetovy algoritmus jako predtim
# generujeme vazene udalosti, proto je dulezite mit spravnou vahu
# vaha = pythia.infoPython().weight()

# muzeme naplnit histogramy napr.
"""
    for jet in jety:
        hist_pt.Fill(jet.pt(), vaha)
        hist_y.Fill(jet.rapidity(), vaha)
        hist_phi.Fill(jet.phi(), vaha)
"""

# pokud mame alespon 2 jety, muzeme zpetne spocitat x1, x2
# sqrt(s)=13600 GeV je tezistova energie srazky
# x1 = (jet0.pt*exp(jet0.y)+jet1.pt*exp(jet1.y))/sqrt(s)
# x2 = (jet0.pt*exp(-jet0.y)+jet1.pt*exp(-jet1.y))/sqrt(s)
#        hist_x1.Fill(x1,vaha)
#        hist_x2.Fill(x2,vaha)

# vykresleme vsechno do 1 canvasu rozdeleneho na 2x2
# novycanvas = TCanvas("jety")
# novycanvas.Divide(2,2)
# novycanvas.cd(1)
# ....
#novycanvas.Update()

