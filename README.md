# casticovapraha
Studenstký projekt pro Částicovou Prahu 2024

## Abstrakt 
Při proton-protonových srážkách zkoumáme vnitřní strukturu protonu a její detaily na rozměrech 10^-17m a menších. To nejde udělat žádným optickým mikroskopem ani jiným nástrojem než je urychlovač a srážeč částic. Při těchto srážkách se často produkují tzv. jety - typické, kolimované spršky částic. Studenti se seznámí s tím, jak jetový algoritmus umožňuje charakterizovat jednotlivé srážky a co produkce jetů vypovídá o struktuře protonu.

## Cíle

- prezentace, co jsou to jety a jak nám umožňují zkoumat vnitřní strukturu protonu
- seznámení se s prostředím operačního systému Linux
- seznámení se s programovacím jazykem Python
- pokus naprogramovat anti-kt jetový algoritmus
- použiti anti-kt algoritmu (z balíku fastjet) na rekonstrukci jetů z proton-protonových srážek vygenerovaných v Monte Carlo generátoru Pythia


## Getting started/Začínáme

V linuxovém terminálu
```
git clone https://gitlab.fjfi.cvut.cz/zhubacek/casticovapraha.git
cd casticovapraha
source setup.sh
virtualenv casticovapraha_090924
source casticovapraha_090924/bin/activate
pip install fastjet
```
POZN: kdyby nefungovalo, dat pip install fastjet jeste jednou

Pri kazdem spusteni (novem terminalu)
```
cd casticovapraha
source setup.sh
source casticovapraha_090924/bin/activate

```

## Reseni
V adresari Reseni jsou mozne priklady, jak by vysledek mohl vypadat
Daji se spustit primo z terminalu:
```
./1_XXX_reseni.py
```

## Authors and acknowledgment
Zdeněk Hubáček (zdenek.hubacek@fjfi.cvut.cz) 2024

