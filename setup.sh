export LCGENV_PATH=/cvmfs/sft.cern.ch/lcg/releases
export PATH=/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest:${PATH}
eval "`lcgenv x86_64-el9-gcc13-opt all`"

# try LCG_105
eval "`lcgenv -p LCG_105 x86_64-el9-gcc13-opt ROOT`"
#eval "`lcgenv -p LCG_105 x86_64-el9-gcc13-opt fastjet`"
eval "`lcgenv -p LCG_105 x86_64-el9-gcc13-opt pythia8`"
eval "`lcgenv -p LCG_105 x86_64-el9-gcc13-opt virtualenv`"

# setup Pythia installation, will use precompiled version from LCG
export PYTHONPATH=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/pythia8/310-2f242/x86_64-el9-gcc13-opt/lib:$PYTHONPATH
export PYTHIA8DATA=/cvmfs/sft.cern.ch/lcg/releases/LCG_105/MCGenerators/pythia8/310/x86_64-el9-gcc13-opt/share/Pythia8/xmldoc/
