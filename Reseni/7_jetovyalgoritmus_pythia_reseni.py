#! /usr/bin/env python
# a na zaver bych chtel zkusit pustit jetovy algoritmus napr. na par eventu z Monte Carlo generatoru Pythia

from fastjet import PseudoJet,JetDefinition, ClusterSequence, sorted_by_pt, antikt_algorithm
from ROOT import gRandom, TH1D, TCanvas, gApplication, gStyle, gPad
from pythia8 import *
import math

# nase nastaveni
poceteventu = 10000
dR = 0.4
minpt = 100

# nastaveni Monte Carlo generatoru Pythia
# budeme generovat "Hard QCD"/"tvrde" srazky, ve kterych vznikaji jety

pythia = Pythia("", False)
pythia.readString("Beams:eCM = 13600.")
pythia.readString("HardQCD:all = on")
pythia.readString("PhaseSpace:pTHatMin = 1000.")
pythia.readString("PhaseSpace:bias2Selection = on")

#inicializace
pythia.init()
fastjetalgoritmus = JetDefinition(antikt_algorithm, dR)

#histogramy
hist_pt  = TH1D("histogram_pt","histogram_pt",50,0,5000)
hist_y   = TH1D("histogram_y","histogram_y",100,-5,5)
hist_phi = TH1D("histogram_phi","histogram_phi",63,0,6.28)

hist_x1  = TH1D("histogram_x1","histogram_x1",50,0,1)
hist_x2  = TH1D("histogram_x2","histogram_x2",50,0,1)

hist_pt.Sumw2()
hist_y.Sumw2()
hist_phi.Sumw2()
hist_x1.Sumw2()
hist_x2.Sumw2()

for iEvent in range(poceteventu):
    if not pythia.next(): continue;
    if (iEvent % 1000 == 0):
        print ("Generuji event: ",iEvent)
    castice = []
    if (iEvent % 1000 == 0):
        print("Pocet vsech castic: ",pythia.event.size())
    for particle in pythia.event:
        #ignore neutrinos and muons
        if particle.isFinal() and abs(particle.id())!=12 and abs(particle.id())!=13 and abs(particle.id())!=14 and abs(particle.id())!=16:
            castice.append(PseudoJet(particle.px(),particle.py(),particle.pz(),particle.e()))
    if (iEvent % 1000 == 0):
        print ("Pocet castic ",len(castice))

    clusters = ClusterSequence(castice,fastjetalgoritmus)
    jety = sorted_by_pt(clusters.inclusive_jets(minpt))
    if (iEvent % 1000 == 0):
        print ("Pocet jetu: ",len(jety))
    vaha = pythia.infoPython().weight()
    for jet in jety:
        hist_pt.Fill(jet.pt(), vaha)
        hist_y.Fill(jet.rapidity(), vaha)
        hist_phi.Fill(jet.phi(), vaha)
    if (len(jety)>=2):
        #print (jety[0].pt(), jety[0].rapidity(), jety[1].pt(), jety[1].rapidity(), 13600
        x1 = (jety[0].pt()*math.exp(jety[0].rapidity())+jety[1].pt()*math.exp(jety[1].rapidity()))/13600
        x2 = (jety[0].pt()*math.exp(-jety[0].rapidity())+jety[1].pt()*math.exp(-jety[1].rapidity()))/13600
        #print (x1,x2)
        hist_x1.Fill(x1,vaha)
        hist_x2.Fill(x2,vaha)
        
novycanvas = TCanvas("jety")
novycanvas.Divide(2,2)
novycanvas.cd(1)
hist_pt.Draw("HIST")
gPad.SetLogy()
novycanvas.cd(2)
hist_y.Draw("HIST")
novycanvas.cd(3)
hist_phi.Draw("HIST")
novycanvas.cd(4)
hist_x1.Draw("HIST")
hist_x2.SetLineColor(2)
hist_x2.Draw("SAMEHIST")

novycanvas.Update()

# tohle, aby se mi to nezavrelo
gApplication.Run()
