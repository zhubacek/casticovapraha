#! /usr/bin/env python

from fastjet import PseudoJet
from math import pi,sqrt

# priklad 4 vektor, trida fastjet.Pseudojet
# (px, py, pz, E)

vektor1 = PseudoJet(100,0,0,200)
vektor2 = PseudoJet(100,100,0,200)

#vzdalenost mezi 2 jety - dR^2 = (y1-y2)^2 + (phi1-phi2)^2

dy = vektor1.rapidity()-vektor2.rapidity()
dphi = vektor1.phi()-vektor2.phi()

# to nebude uplne fungovat - chceme dphi mezi -pi a pi
# python: if (podminka): neco 
if (dphi > pi):
    dphi -= 2*pi
if (dphi < -pi):
    dphi += 2*pi

dr = sqrt(dy*dy + dphi*dphi)
print ("Delta R ", dr, " dy: ",dy, " dphi: ",dphi)

#jake musi byt px, py,pz,E, aby 2 castice byly mene nez R=0.4 od sebe?

# abychom to nemuseli porad pocitat
dr2 = vektor1.delta_R(vektor2)
print ("Delta R z PseudoJetu: ",dr2)
