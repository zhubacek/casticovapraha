#! /usr/bin/env python

from fastjet import PseudoJet

# priklad 4 vektor, trida fastjet.Pseudojet
# (px, py, pz, E)

vektor1 = PseudoJet(100,0,0,100)
vektor2 = PseudoJet(-100,0,0,100)

#4 vektory muzu scitat, odecitat atd..

vektor3 = vektor1+vektor2

print("Pricna hybnost: ", vektor3.pt())
print("Rapidita: ",vektor3.rapidity())
print("Azimutalni uhel: ", vektor3.phi())
print("Hmotnost: ",vektor3.m())

