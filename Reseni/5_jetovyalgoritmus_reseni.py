#! /usr/bin/env python

from fastjet import PseudoJet, JetDefinition, antikt_algorithm
# ROOT nema rad from ROOT import *
from ROOT import gRandom, TH2D, TCanvas, gApplication, gStyle

#udelame si pole (vice prvku stejneho typu)
vektory = []
for i in range(1000):
    pt = gRandom.Gaus(1000,30)
    mass = gRandom.Gaus(20,10)
    if (mass < 0):
        mass = 0
    phi = gRandom.Uniform(0,6.28)
    rapidity = gRandom.Gaus(0,2)
    vektor = PseudoJet()
    vektor.reset_PtYPhiM(pt,rapidity,phi,mass)
    vektory.append(vektor)

# kontrola
print("Pocet vygenerovanych castic ", len(vektory))

# jak zkusit nakreslit?
# tohle je histogram (=graf cetnosti), ale nam pomuze k jednoduche vizualizaci
# na osu x dame rapiditu, na osy y phi jednotlivych castic
# na osu z dame pricnou hybnost pt
rozdelenicastic = TH2D("zobrazenicastic","zobrazenicastic",80,-4,4,63,0,6.28)
for vektor in vektory:
    rozdelenicastic.Fill(vektor.rapidity(),vektor.phi(),vektor.pt())

# zkusime nakreslit na ROOT TCanvas
novycanvas = TCanvas("castice")
rozdelenicastic.Draw("COLZ")
# muzu popsat osy
rozdelenicastic.GetXaxis().SetTitle("rapidita")
rozdelenicastic.GetYaxis().SetTitle("phi")
rozdelenicastic.SetStats(0)

novycanvas.Update()

#ale ted bych chtel zkusit, jestli v tomto velkem mnozstvi castic nejsou vyznamne smery ("jety")
jetovyalgoritmus = JetDefinition(antikt_algorithm, 0.4)
jety = jetovyalgoritmus(vektory)

#a zkusme si to znovu nakreslit
jetovycanvas = TCanvas("jety")
rozdelenijetu = TH2D("zobrazenijetu","zobrazenijetu",80,-4,4,63,0,6.28)
for jet in jety:
    rozdelenijetu.Fill(jet.rapidity(),jet.phi(),jet.pt())
rozdelenijetu.Draw("COLZ")
# muzu popsat osy
rozdelenijetu.GetXaxis().SetTitle("rapidita")
rozdelenijetu.GetYaxis().SetTitle("phi")
rozdelenijetu.SetStats(0)
jetovycanvas.Update()

#co kdybych chtel 'vetsi jety'
jetovyalgoritmus2 = JetDefinition(antikt_algorithm, 0.8)
jety2 = jetovyalgoritmus2(vektory)

#a zkusme si to znovu nakreslit
jetovycanvas2 = TCanvas("jety2")
rozdelenijetu2 = TH2D("zobrazenijetu2","zobrazenijetu2",80,-4,4,63,0,6.28)
for jet in jety2:
    rozdelenijetu2.Fill(jet.rapidity(),jet.phi(),jet.pt())
rozdelenijetu2.Draw("COLZ")
# muzu popsat osy
rozdelenijetu2.GetXaxis().SetTitle("rapidita")
rozdelenijetu2.GetYaxis().SetTitle("phi")
rozdelenijetu2.SetStats(0)
jetovycanvas2.Update()

# tohle, aby se mi to nezavrelo
gApplication.Run()
