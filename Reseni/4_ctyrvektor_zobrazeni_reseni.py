#! /usr/bin/env python

from fastjet import PseudoJet
# ROOT nema rad from ROOT import *
from ROOT import gRandom, TH2D, TCanvas, gApplication, gStyle

# priklad 4 vektor, trida fastjet.Pseudojet

#udelame si pole (vice prvku stejneho typu)
vektory = []

#smycka (loop), vytvorime 100 castic (ctyrvektoru)
for i in range(100):
    pt = gRandom.Gaus(100,30)
    mass = gRandom.Gaus(20,10)
    if (mass < 0):
        mass = 0
    phi = gRandom.Uniform(0,6.28)
    rapidity = gRandom.Uniform(-4,4)
    vektor = PseudoJet()
    vektor.reset_PtYPhiM(pt,rapidity,phi,mass)
    vektory.append(vektor)

# kontrola
print("Pocet vygenerovanych castic ", len(vektory))

# jak zkusit nakreslit?
# tohle je histogram (=graf cetnosti), ale nam pomuze k jednoduche vizualizaci
# na osu x dame rapiditu, na osy y phi jednotlivych castic
# na osu z dame pricnou hybnost pt
rozdeleni = TH2D("zobrazenicastic","zobrazenicastic",80,-4,4,63,0,6.28)
for vektor in vektory:
    rozdeleni.Fill(vektor.rapidity(),vektor.phi(),vektor.pt())

# zkusime nakreslit na ROOT TCanvas
novycanvas = TCanvas("obrazek1")
rozdeleni.Draw("COLZ")
# muzu popsat osy
rozdeleni.GetXaxis().SetTitle("rapidita")
rozdeleni.GetYaxis().SetTitle("phi")
rozdeleni.SetStats(0)

novycanvas.Update()


# tohle, aby se mi to nezavrelo
gApplication.Run()
