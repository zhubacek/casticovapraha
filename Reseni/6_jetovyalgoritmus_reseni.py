#! /usr/bin/env python
# tady bych chtel misto fastjetu zkusit udelat vlastni algoritmus
from fastjet import PseudoJet, JetDefinition, antikt_algorithm, ClusterSequence, sorted_by_pt
# ROOT nema rad from ROOT import *
from ROOT import gRandom, TH2D, TCanvas, gApplication, gStyle


def jetovyalgoritmus(castice,dR,minpt, debug = False):
    #nebudeme to komplikovat, predpokladejme, ze na vstupu bude list 4vektoru (PseudoJet) a jeden parametr, polomer

    jets = []
    while (len(castice)):
        if (debug):
            print ("Zbyva castic: ",len(castice))
        if (len(castice) % 100 == 0):
            print ("Zbyva castic: ",len(castice))
        #udelame si n x n matici vzdalenosti
        vzdalenosti = [[None for _ in range(len(castice))] for _ in range(len(castice))]

        #zkusme naplnit
        for i in range(len(castice)):
            for j in range(len(castice)):
                if (j<i):
                    dr=castice[i].delta_R(castice[j])
                    dist = min(pow(castice[i].pt(),-2),pow(castice[j].pt(),-2))*(dr*dr)/(dR*dR)
                    #print (i,j," pt i: ",castice[i].pt(), " ptj ",castice[j].pt(), " dr: ",dr, " with dR ",dR)
                    vzdalenosti[i][j] = dist
            vzdalenosti[i][i]=(pow(castice[i].pt(),-2))

        #pojdme zkusit najit minimum
        min_value = float('inf')
        min_i, min_j = -1, -1

        #for row in vzdalenosti:
        #    print (row)
        
        for i in range(len(vzdalenosti)):
            for j in range(len(vzdalenosti)):
                if (vzdalenosti[i][j] is not None and vzdalenosti[i][j] < min_value):
                    min_value = vzdalenosti[i][j]
                    min_i, min_j = i, j
        if (debug):
            print("Minimum distance: ",min_value, " i: ",min_i," j: ",min_j, " pti ",castice[min_i].pt(), " ptj ",castice[min_j].pt())
                
        if (min_i==min_j):
            #tohle uz je minimum, z tehle castice udelame samostatny jet
            if (debug):
                print("Moving ",min_i," to jets")
            jets.append(castice[min_i])
            castice.remove(castice[min_j])
        else:
            if (debug):
                print("Merging ",min_i," and ",min_j)
            #spojime min_i a min_j dohromady
            par = castice[min_i]+castice[min_j]
            #pridame do seznamu castic a odebereme puvodni
            castice.append(par)
            castice.remove(castice[min_i])
            castice.remove(castice[min_j])

    #jeste preskladame podle pt od nejvyssiho
    print ("Orig jet size: ",len(jets))
    jets.sort(key=lambda obj: obj.pt(), reverse=True)
    for jet in range(len(jets)):
        if (jets[jet].pt()<minpt):
            jets.remove(jet)

    print ("Final jet size: ",len(jets))
    return jets

#udelame si pole (vice prvku stejneho typu)
vstupnicastice = []
for i in range(100):

    pt = gRandom.Gaus(1000,500)
    if (pt < 0.):
        pt = -pt
    mass = gRandom.Gaus(20,10)
    if (mass < 0):
        mass = 0
    phi = gRandom.Uniform(0,6.28)
    rapidity = gRandom.Gaus(0,2)
    castice = PseudoJet()
    castice.reset_PtYPhiM(pt,rapidity,phi,mass)
    vstupnicastice.append(castice)

# kontrola
print("Pocet vygenerovanych castic ", len(vstupnicastice))

# jak zkusit nakreslit?
# tohle je histogram (=graf cetnosti), ale nam pomuze k jednoduche vizualizaci
# na osu x dame rapiditu, na osy y phi jednotlivych castic
# na osu z dame pricnou hybnost pt
rozdelenicastic = TH2D("zobrazenicastic","zobrazenicastic",80,-4,4,63,0,6.28)
for castice in vstupnicastice:
    rozdelenicastic.Fill(castice.rapidity(),castice.phi(),castice.pt())

# zkusime nakreslit na ROOT TCanvas
novycanvas = TCanvas("castice")
rozdelenicastic.Draw("COLZ")
# muzu popsat osy
rozdelenicastic.GetXaxis().SetTitle("rapidita")
rozdelenicastic.GetYaxis().SetTitle("phi")
rozdelenicastic.SetStats(0)

novycanvas.Update()

#ale ted bych chtel zkusit, jestli v tomto velkem mnozstvi castic nejsou vyznamne smery ("jety")

#puvodni fastjet
dR = 0.4
min_pt = 10
fastjetalgoritmus = JetDefinition(antikt_algorithm, dR)
clusters = ClusterSequence(vstupnicastice,fastjetalgoritmus)
jety = sorted_by_pt(clusters.inclusive_jets(10.))

#zkusime vlastni a porovname
tiskvseho = False
jety2 = jetovyalgoritmus(vstupnicastice,dR,min_pt, tiskvseho)
print ("Fastjet nasel ",len(jety), " jetu")
print ("Nase metoda nasla ",len(jety2), " jetu")

print ("Fastjet")
for index,jet in enumerate(jety):
    print (index, jet.pt(), jet.rapidity(), jet.phi())
print ("Nas algoritmus")
for index,jet in enumerate(jety2):
    print (index,jet.pt(), jet.rapidity(), jet.phi())



#a zkusme si to znovu nakreslit
jetovycanvas = TCanvas("jety")
rozdelenijetu = TH2D("zobrazenijetu","zobrazenijetu",80,-4,4,63,0,6.28)
for jet in jety:
    rozdelenijetu.Fill(jet.rapidity(),jet.phi(),jet.pt())
rozdelenijetu.Draw("COLZ")
# muzu popsat osy
rozdelenijetu.GetXaxis().SetTitle("rapidita")
rozdelenijetu.GetYaxis().SetTitle("phi")
rozdelenijetu.SetStats(0)
jetovycanvas.Update()

#a zkusme si to znovu nakreslit
jetovycanvas2 = TCanvas("jety2")
rozdelenijetu2 = TH2D("zobrazenijetu2","zobrazenijetu2",80,-4,4,63,0,6.28)
for jet in jety2:
    rozdelenijetu2.Fill(jet.rapidity(),jet.phi(),jet.pt())
rozdelenijetu2.Draw("COLZ")
# muzu popsat osy
rozdelenijetu2.GetXaxis().SetTitle("rapidita")
rozdelenijetu2.GetYaxis().SetTitle("phi")
rozdelenijetu2.SetStats(0)
jetovycanvas2.Update()

# tohle, aby se mi to nezavrelo
gApplication.Run()
