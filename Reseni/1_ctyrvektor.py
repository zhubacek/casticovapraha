#! /usr/bin/env python

from fastjet import PseudoJet

# priklad ctyrvektor, trida fastjet.Pseudojet
# fastjet konvence (px, py, pz, E)

# zkuste si libovolny vektor

vektor1 = PseudoJet(100,100,100,200)

#zkusit se podivat na vlastnosti
#pricna hybnost pT:
print ("Pricna hybnost: ", vektor1.pt())

#rapidita, azimutalni uhel
print("Rapidita: ",vektor1.rapidity())
#azimutalni uhel mezi 0 a 2pi (muze byt taky -pi,pi)
#vektor1.phi_std()
print("Azimutalni uhel: ", vektor1.phi())

#invariantni hmotnost
print("Hmotnost: ",vektor1.m())
